﻿using System;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Serializable]
    private struct CameraZoomData
    {
        public float YPos => yPosition;
        public float XRot => xRotation;
        public float Fov => fov;
        
        [SerializeField] private float yPosition;
        [SerializeField] private float xRotation;
        [SerializeField] private float fov;

        public CameraZoomData(float yPos, float xRot, float fov)
        {
            yPosition = yPos;
            xRotation = xRot;
            this.fov = fov;
        }
    }
    
    private const int MIDDLE_MOUSE_BUTTON = 2;
    private const float FRAME_WIDTH_PERCENTAGE = 0.01f;
    
    public static CameraController Instance { get; private set; }
    public Camera Camera => camera;
    
    [SerializeField] private float moveSpeed = 30f;
    [SerializeField] private CameraZoomData zoomedInSetting = new CameraZoomData(-6, 40, 60);
    [SerializeField] private float zoomSpeed;

    private new Camera camera;
    private int frameWidth;
    private CameraZoomData zoomedOutSetting;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        camera = GetComponentInChildren<Camera>();
        frameWidth = Mathf.RoundToInt(Screen.width * FRAME_WIDTH_PERCENTAGE);
        zoomedOutSetting = new CameraZoomData(camera.transform.localPosition.y, camera.transform.localEulerAngles.x, 
            camera.fieldOfView);
    }

    private void LateUpdate()
    {
        CameraMovement();
        CameraZoom();
    }

    private void CameraMovement()
    {
        Vector3 movement = Vector3.zero;
        if (Input.GetMouseButton(MIDDLE_MOUSE_BUTTON))
        {
            Cursor.visible = false;
            Vector2 difference = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
            movement = new Vector3(difference.x, 0, difference.y) * (moveSpeed * Time.deltaTime);
            Cursor.lockState = CursorLockMode.Locked;
        }
        else if (Input.GetMouseButtonUp(MIDDLE_MOUSE_BUTTON))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");
    
#if !UNITY_EDITOR
            if (Mathf.Approximately(horizontal, 0) && Mathf.Approximately(vertical, 0))
            {
                // movement using mouse
                horizontal = Input.mousePosition.x >= Screen.width - frameWidth ? 1 :
                    Input.mousePosition.x <= frameWidth ? -1 : 0;
                vertical = Input.mousePosition.y >= Screen.height - frameWidth ? 1 :
                    Input.mousePosition.y <= frameWidth ? -1 : 0;
            }
#endif
            
            movement = new Vector3(horizontal, 0, vertical) * (moveSpeed * Time.deltaTime);
        }
        
        transform.Translate(movement, Space.Self);
    }
    
    // TODO clean this up and make it smooth
    private void CameraZoom()
    {
        float scrollAmount = Input.mouseScrollDelta.y;
        Vector3 targetPos;
        Vector3 targetRot;
        float posDistance = Mathf.Abs(zoomedInSetting.YPos - zoomedOutSetting.YPos);
        float rotDistance = Mathf.Abs(zoomedInSetting.XRot - zoomedOutSetting.XRot);
        float fovDistance = Mathf.Abs(zoomedInSetting.Fov - zoomedOutSetting.Fov);
        if (scrollAmount > 0)
        {
            targetPos = new Vector3(0, zoomedInSetting.YPos, 0);
            camera.transform.localPosition =
                Vector3.MoveTowards(camera.transform.localPosition, targetPos, Time.deltaTime * zoomSpeed * posDistance);
            targetRot = new Vector3(zoomedInSetting.XRot, 0, 0);
            camera.transform.localEulerAngles =
                Vector3.MoveTowards(camera.transform.localEulerAngles, targetRot, Time.deltaTime * zoomSpeed * rotDistance);
            camera.fieldOfView =
                Mathf.MoveTowards(camera.fieldOfView, zoomedInSetting.Fov, Time.deltaTime * zoomSpeed * fovDistance);
        }
        else if (scrollAmount < 0)
        {
            targetPos = new Vector3(0, zoomedOutSetting.YPos, 0);
            camera.transform.localPosition =
                Vector3.MoveTowards(camera.transform.localPosition, targetPos, Time.deltaTime * zoomSpeed * posDistance);
            targetRot = new Vector3(zoomedOutSetting.XRot, 0, 0);
            camera.transform.localEulerAngles =
                Vector3.MoveTowards(camera.transform.localEulerAngles, targetRot, Time.deltaTime * zoomSpeed * rotDistance);
            camera.fieldOfView =
                Mathf.MoveTowards(camera.fieldOfView, zoomedOutSetting.Fov, Time.deltaTime * zoomSpeed * fovDistance);
        }
    }
}
