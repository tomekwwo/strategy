﻿using System.Collections;
using JetBrains.Annotations;
using UnityEngine;

public class AttackerUnit : Unit, IAttacker, IDamagable
{
    public Transform Transform => transform;

    public float MaxHp => maxHp;
    public float CurrentHp { get; private set; }
    public bool IsDead => CurrentHp <= 0;

    public Vector2 Damage => damage;
    public float AttackRange => attackRange;
    public float AttackSpeed => attackSpeed;

    [SerializeField] private float maxHp;
    [SerializeField] private float attackRange;
    [SerializeField] private float attackSpeed;
    [SerializeField] private Vector2 damage;
    [SerializeField] private HpBar hpBar;

    private IDamagable currentTarget;
    private float attackTimer = 0;

    private int animationAttackHash = Animator.StringToHash("Attack");
    private int animationDieHash = Animator.StringToHash("Die");

    protected override void Awake()
    {
        base.Awake();
        
        CurrentHp = MaxHp;
        hpBar.SetValue(1);
        hpBar.SetColor(GameController.Instance.TeamColors[team]);
    }

    public override void CancelOrders()
    {
        base.CancelOrders();
        
        animator.ResetTrigger(animationAttackHash);
    }

    public void Attack(IDamagable target)
    {
        CancelOrders();

        if (target == this)
        {
            return;
        }
        
        currentTarget = target;
        FollowTarget(target.Transform, AttackRange);
        StartCoroutine(AttackLoop(target));
    }

    private IEnumerator AttackLoop(IDamagable target)
    {
        Status = UnitStatus.Attacking;
        
        while (true)
        {
            if (target.IsDead)
            {
                unitsSeen.Remove(target as Unit);
                PickTarget();
            }

            attackTimer -= Time.deltaTime;
            if (attackTimer <= 0 && Vector3.Distance(transform.position, target.Transform.position) <= attackRange)
            {
                attackTimer = 1 / attackSpeed;
                animator.SetTrigger(animationAttackHash);
            }
            yield return null;
        }
    }

    private void PickTarget()
    {
        foreach (var unit in unitsSeen)
        {
            if (unit.Team != team && unit is IDamagable target)
            {
                Attack(target);
                return;
            }
        }
        
        CancelOrders();
    }

    /// <summary>
    /// Method invoked by attack animation.
    /// </summary>
    /// <param name="whichAttack">Each animation cycle is one shield attack and one sword attack. 0 is shield, 1 is sword.</param>
    [UsedImplicitly]
    public void DealDamage(int whichAttack)
    {
        DealDamage(currentTarget);
    }

    public void DealDamage(IDamagable target)
    {
        float damageDealt = Random.Range(damage.x, damage.y);
        target.TakeDamage(damageDealt);
    }

    public void TakeDamage(float value)
    {
        CurrentHp -= value;
        hpBar.SetValue(Mathf.Clamp01(CurrentHp / MaxHp));
        if (CurrentHp <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        CancelOrders();
        animator.SetTrigger(animationDieHash);
        CurrentHp = 0;
        agent.enabled = false;
        unitCollider.enabled = false;
        hpBar.gameObject.SetActive(false);
        GameController.Instance.UnitDied(this);
    }

    public override void UnitSeen(Unit unit)
    {
        base.UnitSeen(unit);

        if (currentTarget == null && unit.Team != team && (Status == UnitStatus.Idle || Status == UnitStatus.Attacking) && 
            unit is IDamagable target)
        {
            Attack(target);
        }
    }

    protected override void ArrivedAtDestination()
    {
        base.ArrivedAtDestination();
        
        PickTarget();
    }
}
