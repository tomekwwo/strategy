﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Unit : MonoBehaviour
{
    private const float ARRIVED_DISTANCE = 0.1f;
    
    public bool IsInPlayerTeam => team == GameController.PLAYER_TEAM_NR;
    public int Team => team;
    public UnitStatus Status { get; set; }
    
    [SerializeField] private GameObject selectionMarker;
    [SerializeField] private SphereCollider fieldOfViewCollider;
    
    [SerializeField] protected int team = 0;
    [SerializeField] protected float sightDistance = 10;
    protected NavMeshAgent agent;
    protected Animator animator;
    protected Collider unitCollider;
    protected HashSet<Unit> unitsSeen = new HashSet<Unit>();
    protected bool arrived = false;

    private int animationMoveSpeedHash = Animator.StringToHash("MoveSpeed");

    protected virtual void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        unitCollider = GetComponent<Collider>();
        fieldOfViewCollider.radius = sightDistance;
        
        GameController.Instance.RegisterUnit(this);
    }

    private void Update()
    {
        if (animator)
        {
            float agentSpeed = agent.velocity.magnitude / agent.speed;
            animator.SetFloat(animationMoveSpeedHash, agentSpeed);
        }

        if (agent.hasPath && agent.remainingDistance <= ARRIVED_DISTANCE && !arrived)
        {
            ArrivedAtDestination();
        }
    }

    public virtual void OnSelected(bool selected)
    {
        selectionMarker.SetActive(selected);
    }

    #region Commands

    public void Goto(Vector3 destination)
    {
        CancelOrders();

        arrived = false;
        Status = UnitStatus.Running;
        agent.stoppingDistance = 0;
        agent.velocity = Vector3.zero;
        agent.SetDestination(destination);
    }

    public void FollowTarget(Transform target, float stoppingDistance = -1)
    {
        CancelOrders();

        Status = UnitStatus.Following;
        agent.stoppingDistance = stoppingDistance > 0 ? stoppingDistance : agent.radius * 3;
        agent.SetDestination(target.position);
        StartCoroutine(RefreshFollowTargetPosition(target));
    }

    public virtual void CancelOrders()
    {
        Status = UnitStatus.Idle;
        StopAllCoroutines();
    }

    private IEnumerator RefreshFollowTargetPosition(Transform target)
    {
        while (true)
        {
            yield return null;
            agent.SetDestination(target.position);
        }
    }

    #endregion

    public virtual void UnitSeen(Unit unit)
    {
        unitsSeen.Add(unit);
    }

    public virtual void UnitLostFromSight(Unit unit)
    {
        if (unitsSeen.Contains(unit))
        {
            unitsSeen.Remove(unit);
        }
    }

    protected virtual void ArrivedAtDestination()
    {
        arrived = true;
        Status = UnitStatus.Idle;
    }
}
