﻿using UnityEngine;

public interface IAttacker
{
    Transform Transform { get; }
    Vector2 Damage { get; }
    float AttackRange { get; }
    float AttackSpeed { get; }
    
    void Attack(IDamagable target);
    void DealDamage(IDamagable target);
}
