﻿using UnityEngine;

public interface IDamagable
{
    Transform Transform { get; }
    
    float MaxHp { get; }
    float CurrentHp { get; }
    bool IsDead { get; }

    void TakeDamage(float value);
    void Die();
}
