﻿using UnityEngine;

public class Marker : MonoBehaviour
{
    [SerializeField] private SpriteRenderer[] sprites;

    public void SetColor(Color color)
    {
        foreach (var sprite in sprites)
        {
            sprite.color = color;
        }
    }
}
