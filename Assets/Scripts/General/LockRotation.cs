﻿using UnityEngine;

public class LockRotation : MonoBehaviour
{
    [SerializeField] private bool takeInitialRotation = false;
    [SerializeField] private Vector3 rotation;
    
    private void Awake()
    {
        if (takeInitialRotation)
        {
            rotation = transform.eulerAngles;
        }
    }

    private void LateUpdate()
    {
        Vector3 difference = rotation - transform.eulerAngles;
        transform.Rotate(difference);
    }
}
