﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class RedirectAnimationEvents : MonoBehaviour
{
    [SerializeField] private AttackerUnit targetUnit;

    [UsedImplicitly]
    public void DealDamage(int whichAttack)
    {
        targetUnit.DealDamage(whichAttack);
    }
}
