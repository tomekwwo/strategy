﻿using UnityEngine;

public class OnTriggerEvents : MonoBehaviour
{
    [SerializeField] private Unit targetUnit;

    private void OnTriggerEnter(Collider other)
    {
        Unit unit = other.GetComponent<Unit>();
        if (unit)
        {
            targetUnit.UnitSeen(unit);
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        Unit unit = other.GetComponent<Unit>();
        if (unit)
        {
            targetUnit.UnitLostFromSight(unit);
        }
    }
}
