﻿public enum UnitStatus
{
    Idle,
    Running,
    Following,
    Attacking
}