﻿using UnityEngine;
using UnityEngine.UI;

public class SquareSelection : MonoBehaviour
{
    private Vector2 MousePos => new Vector2(Input.mousePosition.x, Input.mousePosition.y - Screen.height);
    private Vector2 CurrentPos => new Vector2(rect.anchoredPosition.x, Mathf.Abs(rect.anchoredPosition.y));
    private Vector2 CurrentSize => new Vector2(Mathf.Abs(rect.sizeDelta.x), Mathf.Abs(rect.sizeDelta.y));
    
    private RectTransform rect;
    private Image image;
    private Vector2 mouseDownPos;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        image = GetComponent<Image>();
        image.enabled = false;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseDownPos = MousePos;
        }
        else if (Input.GetMouseButton(0))
        {
            if (!image.enabled && Vector2.Distance(mouseDownPos, MousePos) > GameController.Instance.MaxClickDistance)
            {
                rect.anchoredPosition = mouseDownPos;
                image.enabled = true;
            }
            SetSizeAndPosition();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (!image.enabled)
            {
                return;
            }

            if (!Input.GetKey(KeyCode.LeftShift))
            {
                GameController.Instance.ClearUnitSelection();
            }

            var units = GameController.Instance.PlayerUnits;
            Camera cam = CameraController.Instance.Camera;
            foreach (var unit in units)
            {
                if (unit is IDamagable damagable && damagable.IsDead)
                {
                    continue;
                }
                
                var screenPoint = cam.WorldToScreenPoint(unit.transform.position);
                if (IsPointInsideRect(screenPoint))
                {
                    GameController.Instance.SetUnitSelection(unit, true);
                }
            }

            image.enabled = false;
        }
    }

    private bool IsPointInsideRect(Vector2 point)
    {
        // invert 'y' coordinate before checking
        point.y = Screen.height - point.y;
        return point.x >= CurrentPos.x && point.y >= Mathf.Abs(CurrentPos.y) &&
               point.x <= CurrentPos.x + CurrentSize.x && point.y <= Mathf.Abs(CurrentPos.y) + CurrentSize.y;
    }

    private void SetSizeAndPosition()
    {
        var pos = mouseDownPos;
        var size = new Vector2(
            MousePos.x - pos.x,
            -MousePos.y + pos.y
            );

        if (size.x < 0)
        {
            size.x = Mathf.Abs(size.x);
            pos.x = MousePos.x;
        }

        if (size.y < 0)
        {
            size.y = Mathf.Abs(size.y);
            pos.y = MousePos.y;
        }

        rect.anchoredPosition = pos;
        rect.sizeDelta = size;
    }
}
