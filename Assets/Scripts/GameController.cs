﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GameController : MonoBehaviour
{
    public const int PLAYER_TEAM_NR = 0;
    public const float RAY_MAX_DISTANCE = 100f;
    public const float NAVMESH_POSITION_MAX_DISTANCE = 5f;
    
    private const float MARKER_LIFE_TIME = .5f;
    
    public static GameController Instance { get; private set; }

    public int MaxClickDistance => maxClickDistance;
    public List<Unit> PlayerUnits => new List<Unit>(playerUnits);
    public Color[] TeamColors => teamColors;

    private bool HasSelectedUnits => selectedUnits != null && selectedUnits.Count > 0;

    [SerializeField] private GameObject markerPrefab;
    [Tooltip("Max distance in pixels which cursor can slide not to be identified as dragging.")]
    [SerializeField] private int maxClickDistance = 6;
    [SerializeField] private Color[] teamColors = {Color.green, Color.red};

    private HashSet<Unit> selectedUnits = new HashSet<Unit>();
    private HashSet<Unit> playerUnits = new HashSet<Unit>();
    private Vector2 mouseDownPos;
    private bool inputLocked = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }
    
    private void Update()
    {
        if (inputLocked)
        {
            return;
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            mouseDownPos = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0) && Vector2.Distance(mouseDownPos, Input.mousePosition) <= maxClickDistance)
        {
            HandleUnitSelection();
        }
        else if (Input.GetMouseButtonDown(1) && HasSelectedUnits) // give commands to selected units
        {
            HandleCommands();
        }
        else if (Input.GetKeyDown(KeyCode.F1))
        {
            SelectAllUnits();
        }
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            ExecuteCommand(new AttackMoveCommand(selectedUnits));
        }
    }

    private void HandleUnitSelection()
    {
        if (Physics.Raycast(GetMouseRay(), out var hitInfo, RAY_MAX_DISTANCE, MasksHelper.UnitsMask))
        {
            Unit unit = hitInfo.collider.GetComponent<Unit>();
            if (unit && unit.IsInPlayerTeam)
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    if (selectedUnits.Contains(unit))
                    {
                        SetUnitSelection(unit, false);
                    }
                    else
                    {
                        SetUnitSelection(unit, true);
                    }
                }
                else
                {
                    ClearUnitSelection();
                    SetUnitSelection(unit, true);
                }
            }
        }
        else
        {
            ClearUnitSelection();
        }
    }

    private void HandleCommands()
    {
        if (Physics.Raycast(GetMouseRay(), out var hitInfo, RAY_MAX_DISTANCE, MasksHelper.TerrainMask | MasksHelper.UnitsMask) && 
            NavMesh.SamplePosition(hitInfo.point, out var hit, NAVMESH_POSITION_MAX_DISTANCE, NavMesh.AllAreas))
        {
            Unit pointedUnit = hitInfo.transform.GetComponent<Unit>();
            if (pointedUnit)
            {
                if (!pointedUnit.IsInPlayerTeam)
                {
                    ExecuteCommand(new AttackCommand(selectedUnits, pointedUnit));
                }
                else
                {
                    ExecuteCommand(new FollowCommand(selectedUnits, pointedUnit));
                }
            }
            else
            {
                ExecuteCommand(new GoToCommand(selectedUnits, hit.position));
            }
        }
    }

    public void ExecuteCommand(ICommand command)
    {
        StartCoroutine(Execute(command));
    }

    private IEnumerator Execute(ICommand command)
    {
        inputLocked = true;
        while (!command.Finished)
        {
            command.Update();
            yield return null;
        }
        inputLocked = false;
    }

    public void RegisterUnit(Unit unit)
    {
        if (unit.IsInPlayerTeam)
        {
            playerUnits.Add(unit);   
        }
    }

    public void ClearUnitSelection()
    {
        foreach (var unit in selectedUnits)
        {
            unit.OnSelected(false);
        }
        selectedUnits.Clear();
    }
    
    public void SetUnitSelection(Unit unit, bool selected)
    {
        if (selected)
        {
            selectedUnits.Add(unit);
        }
        else
        {
            selectedUnits.Remove(unit);
        }
        unit.OnSelected(selected);
    }

    private void SelectAllUnits()
    {
        ClearUnitSelection();
        foreach (var unit in FindObjectsOfType<Unit>())
        {
            SetUnitSelection(unit, true);
        }
    }

    public Ray GetMouseRay()
    {
        return CameraController.Instance.Camera.ScreenPointToRay(Input.mousePosition);
    }

    public void SpawnMarker(Vector3 position, Color color, Transform parent = null)
    {
        Marker marker = Instantiate(markerPrefab).GetComponent<Marker>();
        marker.transform.position = position;
        marker.transform.SetParent(parent);
        marker.SetColor(color);
        Destroy(marker.gameObject, MARKER_LIFE_TIME);
    }

    public void UnitDied(Unit unit)
    {
        if (selectedUnits.Contains(unit))
        {
            SetUnitSelection(unit, false);
            selectedUnits.Remove(unit);
        }

        if (unit.IsInPlayerTeam)
        {
            playerUnits.Remove(unit);
        }
    }
}
