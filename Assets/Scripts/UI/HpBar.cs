﻿using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    [SerializeField] private Image bar;

    public void SetColor(Color color)
    {
        bar.color = color;
    }
    
    public void SetValue(float value)
    {
        bar.fillAmount = Mathf.Clamp01(value);
    }
}
