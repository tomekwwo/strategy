﻿using UnityEngine;

public static class MasksHelper
{
    public static int TerrainMask => 1 << LayerMask.NameToLayer("Terrain");
    public static int UnitsMask => 1 << LayerMask.NameToLayer("Units");
}
