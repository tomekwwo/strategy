﻿using System.Collections.Generic;
using UnityEngine;

public class AttackCommand : ICommand
{
    public bool Finished { get; private set; }

    private ICollection<Unit> units;
    private Unit target;

    public AttackCommand(ICollection<Unit> units, Unit target)
    {
        this.units = units;
        this.target = target;
    }

    public void Update()
    {
        Execute();
    }
    
    private void Execute()
    {
        if (!(target is IDamagable damagable))
        {
            return;
        }
        
        GameController.Instance.SpawnMarker(target.transform.position, Color.red, target.transform);
        foreach (var unit in units)
        {
            if (unit is IAttacker attacker)
            {
                attacker.Attack(damagable);
            }
        }

        Finished = true;
    }
}
