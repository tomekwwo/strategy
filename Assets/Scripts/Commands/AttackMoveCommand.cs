﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AttackMoveCommand : ICommand
{
    public bool Finished { get; private set; }
    private ICollection<Unit> units;

    public AttackMoveCommand(ICollection<Unit> units)
    {
        this.units = units;
    }
    
    public void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            var ray = GameController.Instance.GetMouseRay();
            if (Physics.Raycast(ray, out var hit, GameController.RAY_MAX_DISTANCE, MasksHelper.TerrainMask | MasksHelper.UnitsMask) && 
                NavMesh.SamplePosition(hit.point, out var meshHit, GameController.NAVMESH_POSITION_MAX_DISTANCE, NavMesh.AllAreas))
            {
                Unit unit = hit.collider.GetComponent<Unit>();
                Execute(meshHit.position, unit);
            }
        }

        // cancel command
        if (Input.GetMouseButtonDown(1))
        {
            Finished = true;
        }
    }

    private void Execute(Vector3 position, Unit targetUnit)
    {
        bool targetedUnit = targetUnit != null;
        if (targetedUnit)
        {
            GameController.Instance.ExecuteCommand(new AttackCommand(units, targetUnit));
        }
        else
        {
            foreach (var unit in units)
            {
                unit.Status = UnitStatus.Attacking;
            }
            var gotoCommand = new GoToCommand(units, position, Color.red);
            GameController.Instance.ExecuteCommand(gotoCommand);
        }

        Finished = true;
    }
}
