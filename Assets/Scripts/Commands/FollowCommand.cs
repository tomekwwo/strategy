﻿using System.Collections.Generic;
using UnityEngine;

public class FollowCommand : ICommand
{
    public bool Finished { get; private set; }
    
    private ICollection<Unit> units;
    private Unit target;

    public FollowCommand(ICollection<Unit> units, Unit target)
    {
        this.units = units;
        this.target = target;
    }

    public void Update()
    {
        Execute();
    }
    
    private void Execute()
    {
        foreach (var unit in units)
        {
            unit.FollowTarget(target.transform);
        }

        Finished = true;
    }
}
