﻿public interface ICommand
{
    bool Finished { get; }

    void Update();
}
