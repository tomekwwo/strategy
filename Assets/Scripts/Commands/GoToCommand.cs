﻿using System.Collections.Generic;
using UnityEngine;

public class GoToCommand : ICommand
{
    public bool Finished { get; private set; }

    private ICollection<Unit> units;
    private Vector3 position;
    private Color? markerColor;
    
    public GoToCommand(ICollection<Unit> units, Vector3 position, Color? markerColor = null)
    {
        this.units = units;
        this.position = position;
        this.markerColor = markerColor;
    }

    public void Update()
    {
        Execute();
    }
    
    public void Execute()
    {
        markerColor = markerColor ?? Color.green;
        GameController.Instance.SpawnMarker(position, markerColor.Value);
        int unitsPerRow = Mathf.CeilToInt(Mathf.Sqrt(units.Count));
        int rowCount = Mathf.CeilToInt(units.Count / (float) unitsPerRow);
        float offset = 3;
        float startXOffset = (unitsPerRow - 1) / 2f * offset;
        float startZOffset = (rowCount - 1) / 2f * offset;
        Vector3 startPos = new Vector3(position.x - startXOffset, position.y, position.z - startZOffset);
        Vector3 destination = startPos;
        int columnNumber = 0;
        foreach (var unit in units)
        {
            unit.Goto(destination);
            columnNumber++;
            if (columnNumber >= unitsPerRow)
            {
                destination.x = startPos.x;
                destination.z += offset;
                columnNumber = 0;
            }
            else
            {
                destination.x += offset;   
            }
        }

        Finished = true;
    }
}
